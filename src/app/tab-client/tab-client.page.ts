import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Agency } from '../data/entities/Agency';
import { AgencyServices } from '../services/agency/AgencyServices';

@Component({
  selector: 'app-tab-client',
  templateUrl: 'tab-client.page.html',
  styleUrls: ['tab-client.page.scss']
})
export class TabClientPage {

  public listAgencies: Agency[];

  public averageAge: number;
  public stardardDeviation: number;

  constructor(
    private agencyServices: AgencyServices,
    private navController: NavController
  ) {
    
  }

  ngOnInit() {

    // listAgenciesR.map(_item => {
    //   let agencyObj: Agency = new Agency(
    //     _item.agencia,
    //     _item.distrito,
    //     _item.direccion,
    //     _item.provincia,
    //     _item.departamento,
    //     _item.lat,
    //     _item.lon)
    //   console.log(agencyObj);
    //   this.agencyServices.saveAgency(agencyObj)
    // })

    this.getListAgencies();
  }

  async ionViewDidEnter() {
    this.getListAgencies();
  }

  successGetListAgencies(listAgencies: Agency[]) {
    this.listAgencies = listAgencies;
  };

  goToDetailAgency = (agency: Agency) => this.navController.navigateForward(['tabs/tab-home/detail', { agency: JSON.stringify(agency) }])
  getListAgencies = () => this.agencyServices.getListAgencies().subscribe((_res: Agency[]) => this.successGetListAgencies(_res))


}
