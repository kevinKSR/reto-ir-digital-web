import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormSaveComponent } from './form-save/form-save.component';
import { ListAgenciesComponent } from './list-agencies/list-agencies.component';

@NgModule({
    imports: [
        IonicModule,
        FormsModule,
        CommonModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
    ],
    declarations: [
        FormSaveComponent,
        ListAgenciesComponent
    ],
    exports: [
        FormSaveComponent,
        ListAgenciesComponent
    ],
    providers: []
})


export class ComponentsModule {
}
