import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Agency } from 'src/app/data/entities/Agency';

@Component({
  selector: 'app-list-agencies',
  templateUrl: './list-agencies.component.html',
  styleUrls: ['./list-agencies.component.scss'],
})
export class ListAgenciesComponent implements OnInit {
  @Input() listAgencies: Agency[];
  @Output() emitAgency = new EventEmitter<Agency>();

  constructor() { }


  goToDetailAgency = (agency: Agency) => this.emitAgency.emit(agency);

  ngOnInit() { }

}
