import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AgencyInt } from 'src/app/data/interfaces/AgencyInt';
import { AlertServices } from 'src/app/services/alertServices/AlertServices';
import { AgencyServices } from 'src/app/services/agency/AgencyServices';

@Component({
  selector: 'app-form-save',
  templateUrl: './form-save.component.html',
  styleUrls: ['./form-save.component.scss'],
})
export class FormSaveComponent implements OnInit {

  public formAgency: FormGroup;
  public objAgency: AgencyInt;
  @Input() agency: AgencyInt;

  constructor(
    private formBuilder: FormBuilder,
    private agencyServices: AgencyServices,
    private alertServices: AlertServices
  ) {

    this.formValidator();
  };

  formValidator() {
    this.formAgency = this.formBuilder.group({
      agency: ['', Validators.required],
      department: ['', Validators.required],
      district: ['', Validators.required],
      address: ['', Validators.required],
    });
  };

  ngOnInit() {
    this.objAgency = this.agency;
  }

  updateAgency() {
    console.log(this.objAgency);
    this.agencyServices.updateAgency(this.objAgency, this.objAgency.keyId)
      .then(res => this.successSaveAgency())
      .catch(err => console.log(err));
  };

  successSaveAgency() {
    this.alertServices.createAlert('Confirmation !', 'Agency updated correctly..')
  };

}
