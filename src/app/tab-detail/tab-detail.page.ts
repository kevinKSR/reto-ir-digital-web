import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { elementAt } from 'rxjs/operators';
import { AgencyInt } from '../data/interfaces/AgencyInt';
import { GoogleMapServices } from '../services/googleMap/GoogleMapServices';



@Component({
  selector: 'app-tab-detail',
  templateUrl: './tab-detail.page.html',
  styleUrls: ['./tab-detail.page.scss'],
})
export class TabDetailPage implements OnInit {

  isLoading: boolean;
  agency: AgencyInt;


  @ViewChild('map', { read: ElementRef, static: false }) mapRef: ElementRef;

  constructor(
    private route: ActivatedRoute,
    private googleMapServices: GoogleMapServices
  ) {

    this.route.params.subscribe(params => {
      this.agency = JSON.parse(params["agency"]);
    });
    this.isLoading = false;

  };

  ionViewDidEnter() {
    this.googleMapServices.showMap(this.mapRef);
  }

  ngOnInit() {
  }

}
